# WorkItem Management Platform - OOP Project 

### Overview
We`ve created a console application for basic WorkItem Management. It reads the command from the console and outputs a string, describing the result of the operation.

### Project Information

- Framework: **.NET Core 3.0**
- Language and Version: **C# 8**

### Current functionality

- Create a new person
- Show all people
- Show person`s activity
- Create a new team
- Show all teams
- Show team`s activity
- Add person to team
- Show all team members
- Create a new board in a team
- Show all team boards
- Show board`s activity
- Create a new Bug/Story/Feedback in a board
- Change Priority/Severity/Status of a bug
- Change Priority/Size/Status of a story
- Change Rating/Status of a feedback
- Assign/Unassign work item to a person  Add comment to a work item  List work items with options:
- List all with options:
    - Filter bugs/stories/feedback only 
    - Filter by status and/or assignee
    - Sort by title/priority/severity/size/rating


### Command descriptions:

- **Create Commands**:

    - `create person: {person`s name}` - Creates a new Person with the name provided after the `:`
    - `create bug: {bug`s name},{board in which you wish to create the bug}` - Creates a new Bug with a name, ID and default field values in the specified board.
    - `create story: {story`s name},{board in which you wish to create the story}` - Creates a new Story with a name, ID and default field values in the specified board.
    - `create Feedback: {feedback`s name},{board in which you wish to create the feedback}` - Creates a new Feedback with a name, ID and default field values in the specified board.

- **Add Commands**:   

    - `add description: {can be empty or work item ID}` - Adds a description to the Work Item with the provided ID, or adds description to the last work item added to the database if left empty.
    - `add comment: {Work Item ID},{Comment}` - Adds a comment to the list of Comments for the Work Item with the provided ID.
    - `add to team: {person},{team}` - Adds the specified person to the specified team.
    
- **Change Option Commands**:

    - `change bug option: {ID},{Option you wish to change},{New value for said option}` - Changes the Priority/Severity/Status of a Bug from its current value, to the one provided (respecting the relevant property limitations).
    - `change story option: {ID},{Option you wish to change},{New value for said option}` - Changes the Priority/Size/Status of a Story from its current value, to the one provided (respecting the relevant property limitations).
    - `change feedback option: {ID},{Option you wish to change},{New value for said option}` - Changes the Rating/Status of a Feedback from its current value, to the one provided (respecting the relevant property limitations).


- **Assign/Unassign Commands**:

    - `assign work: {ID},{name of person}` - Assigns the work item with the provided ID to the person with the provided name.
    - `unassign work: {ID},{name of person}` - Unassigns the work item.


- **Show Commands**:

    - `show all people` - Provides a list of all people in the platform.
    - `show all teams` - Provides a list of all teams in the platform.
    - `show members of: {team}` - Provides a list of the members of the specified team.
    - `show activity of: {person`s name}` - Provides a list with the specified person`s activity.
    - `show activity of board: {board`s name}` - Provides a list with the activity within the board.
    - `show activity of team: {team`s name}` - Provides a list with the activity of all members of a team.


- **List Commands**:

    - `list all` - Provides a list of all the Work Items within the platform. 
    - `list all:{work item type}` - Provides a list of all Bugs/Feedbacks/Stories, depending on the type provided.
    - `list all:assigned to:{person`s name}` - Provides a list of all Work Items assigned to the specified person.
    - `list all:{work item type},status:{work item status}` - Provides a list of the work items of the specified type, filtered by the provided status.
    - `list all:{work item type},assignee:{person`s name}` - Provides a list of all the work items assigned to the specified person.
    - `list all:{work item type},status:{work item status},assignee:{person`s name}` - Provides a list of work items of the specified type, filtered for the specified assignee and status.
    - `list all:sorted by:{sorting type}` - Provides a list of work items, sorted by either Title,Priority,Severity,Size, or Rating. If the Work Items do not have the specified property, they are listed alphabetically after the items that do. 