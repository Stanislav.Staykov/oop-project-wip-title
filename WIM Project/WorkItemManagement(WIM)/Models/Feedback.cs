﻿
using WIMProject.Models.Contracts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace WIMProject
{
    public class Feedback : WorkItem, IFeedback, IWorkItem
    {        
        private FeedbackStatusType feedbackStatusType;
        private int rating;

        public Feedback(string feedbackTitle,
                        string feedbackDescription = default, 
                        int rating = default, 
                        FeedbackStatusType feedbackStatusType = default) : base(feedbackTitle)
        {
            this.description = feedbackDescription;
            this.ChangeStatus(feedbackStatusType);
            this.rating = rating;

        }
        public int Rating { get => this.rating; }//?
        public FeedbackStatusType FeedbackStatusType { get => this.feedbackStatusType; }//?
        public void ChangeStatus(FeedbackStatusType feedbackStatusType)
        {
            this.feedbackStatusType = feedbackStatusType;
        }
        public void ChangeRating(int rating)
        {
            this.rating = rating;
        }
    }
}