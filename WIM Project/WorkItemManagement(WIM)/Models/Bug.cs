﻿using System;
using System.Collections.Generic;
using WIMProject.Models.Contracts;

namespace WIMProject
{
    public class Bug : WorkItem, IBug, IWorkItem
    {
        private List<string> stepsToReproduce;
        private PriorityType bugPrio;
        private BugSeverityType bugSever;
        private BugStatusType bugStatus;
     
        public Bug(string bugTitle) 
            : base(bugTitle)
        {
            this.stepsToReproduce = new List<string>();
            this.bugPrio = default;
            this.bugSever = default;
            this.bugStatus = default;
        }


        public List<string> StepsToReproduce 
        {
            get
            {
                return this.stepsToReproduce;
            }
            set
            {
                if (value==null)
                {
                    throw new ArgumentException();
                }
                this.stepsToReproduce = value;
            }
        }


        public PriorityType BugPriorityType 
        {
            get => this.bugPrio;           
        }
        public BugSeverityType BugSeverityType 
        {
            get => this.bugSever;          
        }
        public BugStatusType BugStatusType 
        {
            get => this.bugStatus;           
        }

        public void ChangePriority(PriorityType priority)
        {
            this.bugPrio = priority;
        }
        public void ChangeSeverity(BugSeverityType bugSeverityType)
        {
            this.bugSever = bugSeverityType;
        }
        public void ChangeStatus(BugStatusType bugStatusType)
        {
            this.bugStatus = bugStatusType;
        }

        public void AddStep(string addStep)
        {
            this.stepsToReproduce.Add($"Step: {this.StepsToReproduce.Count+1}:" +
                                        $"{Environment.NewLine} # {addStep}");
        }

    }
}