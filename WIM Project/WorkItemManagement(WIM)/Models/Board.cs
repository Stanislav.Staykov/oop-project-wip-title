﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using WIMProject.Models.Contracts;

namespace WIMProject
{ 
    public class Board : IBoard
    {
        private string name;
        private string team;
        private IList<IWorkItem> boardWorkItems;
        private IList<string> boardHistory;

        public Board(string boardName)
        {
            this.Name = boardName;
            this.boardHistory = new List<string>{$"{Environment.NewLine}Board {this.Name} " +
                                                    $"was created on {DateTime.Now:dddd dd-MM-yyyy}" +
                                                    $" at {DateTime.Now:hh:mm tt}" };

            this.boardWorkItems = new List<IWorkItem>();
            this.team = default;
        }
        public string Name
        {
            get => this.name;
            set
            {
                if (value == null)
                    throw new ArgumentException("Board Name cannot be empty!");

                if (value.Length < 5 || value.Length > 10)
                    throw new ArgumentException("Board Name needs to be between 5 and 10 characters long!");

                this.name = value;
            }
        }

        public string Team 
        {
            get => this.team;
        }


        public IList<IWorkItem> BoardItems
        {
            get => this.boardWorkItems;
        }

        public IList<string> BoardHistory
        {
            get => this.boardHistory;
        }

        public void AddEvent(string msg)
        {
            this.boardHistory.Add(msg);
        }

        public void AddTeam(string teamIn)
        {
            this.team = teamIn;
        }
        public IEnumerator GetEnumerator()
        {
            return new TeamEnum();
        }

        public class TeamEnum : IEnumerator
        {
            public int Current { get; private set; } = 0;

            object IEnumerator.Current => Current;

            public bool MoveNext()
            {
                Current++;
                return true;
            }

            public void Reset()
            {
                Current = 0;
            }
        }
    }
}