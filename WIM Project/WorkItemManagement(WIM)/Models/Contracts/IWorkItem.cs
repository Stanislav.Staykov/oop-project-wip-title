﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Models.Contracts
{
    public interface IWorkItem
    {
        uint ID { get; }
        string Title { get; }        
        public string Description { get; }
        public string Board { get; }
        public IPerson Assignee { get; }
        public IReadOnlyCollection<string> Comments { get; }
        public void AddComment(string msg);
        public void AddDescription(string msg);
        public void AssignPerson(IPerson person);
        public void UnassignPerson(IPerson person);
        public void AddBoard(string boardName);


       // public abstract void ChangePriority(PriorityType priority);
       // public abstract void ChangeSeverity(BugSeverityType bugSeverityType);
       // public abstract void ChangeStatus(BugStatusType bugStatusType);
       // void ChangeSeverity(BugSeverityType bugSeverityType);
    }
}