﻿using System.Collections;
using System.Collections.Generic;

namespace WIMProject.Models.Contracts
{
    public interface ITeam /*: IEnumerable*/
    {
        public string Name { get; }
        IList<IPerson> TeamMembers { get;}
        IList<IBoard> TeamBoards { get; }
        

    }
}