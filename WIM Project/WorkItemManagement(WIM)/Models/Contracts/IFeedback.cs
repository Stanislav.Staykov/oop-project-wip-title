﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Models.Contracts
{
    public interface IFeedback : IWorkItem 
    {       
        int Rating { get; }
        FeedbackStatusType FeedbackStatusType { get; }
        void ChangeStatus(FeedbackStatusType feedbackStatusType);
        void ChangeRating(int rating);
    }
}