﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Models.Contracts
{
    public interface IPerson
    {
        string PersonName { get; }
        IReadOnlyCollection<IWorkItem> WorkItems { get; }
        IReadOnlyCollection<string> ActivityHistory { get; }
        public void AddEvent(string msg);
        public void AddWorkItem(IWorkItem workItem);

    }
}