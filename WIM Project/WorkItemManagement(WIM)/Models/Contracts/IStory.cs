﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Models.Contracts
{
    public interface IStory : IWorkItem
    {
        PriorityType PriorityType { get; }
        StorySizeType SizeType { get; }
        StoryStatusType StoryStatusType { get; }
        void ChangePriority(PriorityType priority);
        void ChangeSizeType(StorySizeType storySizeType);
        void ChangeStatusType(StoryStatusType storyStatusType);
    }
}