﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Models.Contracts
{
    public interface IBug : IWorkItem
    {
        List<string> StepsToReproduce { get; set; }//??????????????
        PriorityType BugPriorityType { get; }
        BugSeverityType BugSeverityType { get; }
        BugStatusType BugStatusType { get; }
        void ChangePriority(PriorityType priority);
        void ChangeSeverity(BugSeverityType bugSeverityType);
        void ChangeStatus(BugStatusType bugStatusType);
        void AddStep(string addStep);
    }
}