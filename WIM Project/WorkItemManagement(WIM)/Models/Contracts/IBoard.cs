﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Models.Contracts
{
    public interface IBoard : IEnumerable
    {
        string Name { get; }
        string Team { get; }
        IList<IWorkItem> BoardItems { get; }
        IList<string> BoardHistory { get; }
        public void AddEvent(string msg);
        public void AddTeam(string teamIn);
    }
}