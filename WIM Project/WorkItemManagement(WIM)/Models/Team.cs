﻿using System.Collections.Generic;
using WIMProject.Models.Contracts;
using System.Linq;
using System.Collections;

namespace WIMProject
{
    public class Team : ITeam
    {
        private string name;
        private IList<IPerson> teamMembers;
        private IList<IBoard> teamBoards;

        public Team(string teamName)
        {
            this.TeamMembers = new List<IPerson>();
            this.TeamBoards = new List<IBoard>();
            this.Name = teamName;
        }


        public string Name
        {
            get
            {
                return this.name;
            }
            private set
            {
                this.name = value;
            }
        }

        public IList<IPerson> TeamMembers
        {
            get
            {
                return this.teamMembers;
            }
            set
            {

                this.teamMembers = value;
            }
        }

        public IList<IBoard> TeamBoards
        {
            get
            {
                return this.teamBoards;
            }
            set
            {
                this.teamBoards = value;
            }
        }

        public void AddPerson(IPerson person)
        {
            teamMembers.Add(person);
        }

      
    }
}