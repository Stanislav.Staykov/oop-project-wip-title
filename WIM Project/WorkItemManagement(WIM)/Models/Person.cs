﻿using System;
using System.Collections.Generic;
using WIMProject.Models.Contracts;

namespace WIMProject.Models
{  
    public class Person : IPerson
    {
        private string personName;
        private List<string> activityHistory;
        private List<IWorkItem> workItems;

        public Person(string personName)
        {
            this.PersonName = personName;
            this.workItems = new List<IWorkItem>();
            this.activityHistory = new List<string>();
        }

        public string PersonName
        {
            get
            {
                return this.personName;
            }
            set
            {
                if (value.Length < 5 || value.Length > 15)
                    throw new ArgumentException("Name must be between 5 and 15 characters!");
                if (value==null)
                    throw new ArgumentException("Name cannot be empty!");

                this.personName = value;
            }
        }

        public  IReadOnlyCollection<IWorkItem> WorkItems { get => this.workItems; }

        public IReadOnlyCollection<string> ActivityHistory { get => this.activityHistory; }

        public void AddEvent(string msg)
        {
            this.activityHistory.Add($"{DateTime.Now}\n {msg}");
        }

        public void AddWorkItem(IWorkItem workItem)
        {
            this.workItems.Add(workItem);
        }
    }
}
