﻿
using System;
using System.Collections.Generic;
using WIMProject.Models.Contracts;

namespace WIMProject
{  
    public class Story : WorkItem, IStory, IWorkItem
    {
        private StorySizeType storySizeType;
        private StoryStatusType storyStatusType;
        private PriorityType storyPriorityType;
        public readonly uint idStory;
        public Story(string storyTitle, 
                     string storyDescription = default, 
                     StorySizeType storySizeType = default, 
                     StoryStatusType storyStatus = default, 
                     PriorityType storyPrio = default, 
                     IPerson storyAssignedTo = default, 
                     string storyComment = default) : base(storyTitle)
        {
            this.storySizeType = storySizeType;
            this.assignee = storyAssignedTo;
            this.storyStatusType = storyStatus;
            this.storyPriorityType = storyPrio;
            this.StoryComment = storyComment;

        }


        public PriorityType PriorityType { get => this.storyPriorityType; }
        public StorySizeType SizeType { get => storySizeType; }
        public StoryStatusType StoryStatusType { get =>storyStatusType; }

        public string StoryComment 
        {
            get
            {
                return string.Join($"{Environment.NewLine}", this.comments);
            }
            set
            {
                this.comments.Add(value);
            }
        }

        public void ChangePriority(PriorityType priority)
        {
            this.storyPriorityType = priority;
        }

        public void ChangeSizeType(StorySizeType storySizeType)
        {
            this.storySizeType = storySizeType;
        }

        public void ChangeStatusType(StoryStatusType storyStatusType)
        {
            this.storyStatusType = storyStatusType;
        }
    }
}