﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject
{
    public enum PriorityType
    {
        High = 0, 
        Medium = 1, 
        Low = 2
    }
}