﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject
{
    public enum BugStatusType
    {
        Active = 0, 
        Fixed = 1
    }
}