﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject
{
    public enum StoryStatusType
    {
        NotDone = 0, 
        InProgress = 1,
        Done = 2
    }
}