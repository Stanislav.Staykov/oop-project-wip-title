﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject
{
    public enum FeedbackStatusType
    {
        New = 0,
        Unscheduled = 1,
        Scheduled = 2,
        Done = 3
    }
}