﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject
{
    public enum BugSeverityType
    {
        Critical = 0,
        Major = 1, 
        Minor = 2
    }
}