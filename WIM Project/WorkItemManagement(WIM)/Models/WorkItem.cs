﻿using System;
using System.Collections.Generic;
using System.Globalization;
using WIMProject.Models.Contracts;

namespace WIMProject
{
    public abstract class WorkItem : IWorkItem
    {
        private static uint currentId = 1;
        protected string title;
        protected string board;
        protected string description;
        protected List<string> comments;
        protected IPerson assignee;    



        public WorkItem(string workItemTitle)
        {
            this.Verificator(workItemTitle);

            this.Title = workItemTitle;
            this.ID = currentId++;
            this.comments = new List<string>();
        }

        public uint ID { get; }


        public string Title
        {
            get => this.title;
            set
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;//?
                this.title = textInfo.ToTitleCase(value);
            }
        }
        public IPerson Assignee
        {
            get => this.assignee;
        }
        public string Description
        {
            get => this.description;
        }
        public IReadOnlyCollection<string> Comments
        {
            get => this.comments;
        }

        public string Board
        {
            get => this.board;
        }

        public void AddBoard(string boardIn)
        {
            this.board = boardIn;
        }

        public void AddComment(string msg)
        {
            this.comments.Add($"Time:{string.Format("hh:mm dd/MM/yyyy",DateTime.Now)}\n\r {msg}");
        }

        public void AddDescription(string msg)
        {
            this.description = $"{DateTime.Now}\n {msg}";
        }

        public void AssignPerson(IPerson person)
        {
            this.assignee = person;
        }

        public void UnassignPerson(IPerson person)
        {
            this.assignee = default;
        }

       


        public void Verificator(string workItemTitle)
        {
            if (workItemTitle.Length < 10 || workItemTitle.Length > 50)
                throw new ArgumentException("The title must be at least 10, and less than 50 characters!");
            if (string.IsNullOrEmpty(workItemTitle))
                throw new ArgumentException("Title cannot be empty!");
        }
    }
}