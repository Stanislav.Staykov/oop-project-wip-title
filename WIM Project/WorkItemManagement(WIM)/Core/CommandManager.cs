﻿
using System;
using System.Linq;
using System.Collections.Generic;
using WIMProject.Core.Contracts;
using WIMProject.Core;
using WIMProject.Commands.Contracts;
using WIMProject.Commands;

namespace WIMProject.Core
{

    public class CommandManager : ICommandManager
    {
        public ICommand ParseCommand(string commandLine)
        {

            //Tyк промених малко инпутът ни - вече очакваме такъв 
            //във формат "команда:параметър1,параметър2,...,параметърN" ;

            var lineParameters = commandLine
                .Trim()
                .Split(":", StringSplitOptions.RemoveEmptyEntries);

            string commandName = lineParameters[0];
            IList<string> commandParameters = new List<string>();

            if (lineParameters.Count()==1)
                commandParameters.Add("");
            else
                commandParameters = lineParameters[1].Split(',', StringSplitOptions.RemoveEmptyEntries);

            return commandName switch
            {
                "create person" => new CreatePersonCommand(commandParameters),
                //В следващите 3 взимаме lineParameters, защото там се съдържа инфото за командата
                "create bug" => new CreateWorkItemCommand(lineParameters),
                "create feedback" => new CreateWorkItemCommand(lineParameters),
                "create story" => new CreateWorkItemCommand(lineParameters),
                "create board" => new CreateBoardInTeamCommand(commandParameters),
                "create team" => new CreateTeamCommand(commandParameters),
                "add description" => new AddDescriptionCommand(commandParameters),
                "add comment" => new AddCommentCommand(commandParameters),
                "change bug option" => new ChangeBugOptionCommand(commandParameters),
                "change story option" => new ChangeStoryOptionCommand(commandParameters),
                "change feedback option" => new ChangeFeedbackOptionCommand(commandParameters),
                "assign work" => new AssignWorkItemToPersonCommand(commandParameters),
                "unassign work" => new UnassignWorkItemToPersonCommand(commandParameters),
                "add to team" => new AddPersonToTeamCommand(commandParameters),
                "show all people" => new ShowAllPeopleCommand(commandParameters),
                "show all teams" => new ShowAllTeamsCommand(commandParameters),
                "show members of" => new ShowAllTeamMembersCommand(commandParameters),
                "show activity of" => new ShowPersonsActivityCommand(commandParameters),
                "show activity of board" => new ShowBoardActivityCommand(commandParameters),
                "show activity of team" => new ShowTeamActivityCommand(commandParameters),
                "list all" => new ListAllCommand(lineParameters),
                _ => throw new InvalidOperationException("Command does not exist")
            };
        }

    }
}