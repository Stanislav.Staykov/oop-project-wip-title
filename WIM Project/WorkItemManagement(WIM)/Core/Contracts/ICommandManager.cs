﻿using WIMProject.Commands.Contracts;

namespace WIMProject.Core.Contracts
{
    public interface ICommandManager
    {
        ICommand ParseCommand(string commandLine);
    }
}

