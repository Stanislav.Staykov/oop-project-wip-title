﻿using System;
using System.Collections.Generic;
using System.Text;
using WIMProject.Models.Contracts;

namespace WIMProject.Core.Contracts
{
    public interface IFactory
    {
        IPerson CreatePerson(string personName);
        ITeam CreateTeam(string teamName);
        IWorkItem CreateBug(string bugName);
        IWorkItem CreateStory(string storyName);
        IWorkItem CreateFeedback(string feedbackName);

        //IList<string> ListPeople () => throw new NotImplementedException("");
        //void ShowPersonActivity() => throw new NotImplementedException("");
    }

}
