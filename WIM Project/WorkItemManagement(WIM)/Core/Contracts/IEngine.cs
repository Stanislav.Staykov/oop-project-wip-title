﻿
namespace WIMProject.Core.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}
