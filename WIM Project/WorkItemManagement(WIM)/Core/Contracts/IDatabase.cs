﻿using System;
using System.Collections.Generic;
using System.Text;
using WIMProject.Models.Contracts;

namespace WIMProject.Core.Contracts
{

        public interface IDatabase
        {
            public IList<IPerson> Person { get; }
            public IList<ITeam> Teams { get; }
            public  IList<IWorkItem> WorkItems { get; }
            public  IList<IBoard> Boards { get; }
            public void Clear();
        }
}
