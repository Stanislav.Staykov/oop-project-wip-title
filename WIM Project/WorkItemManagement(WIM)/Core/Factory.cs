﻿
using System.Collections.Generic;
using System.Linq;
using WIMProject.Core.Contracts;
using WIMProject.Models;
using WIMProject.Models.Contracts;

namespace WIMProject.Core
{
    public class Factory : IFactory
    {
        private static IFactory instance;

        public static IFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Factory();
                }

                return instance;
            }
        }

        public IPerson CreatePerson(string name)
        {
            return new Person(name);
        }
        public IWorkItem CreateBug(string bugName)
        {
            return new Bug(bugName);
        }
        public ITeam CreateTeam(string name)
        {
            return new Team(name);
        }
        public IWorkItem CreateStory(string name)
        {
            return new Story(name);
        }

        public IWorkItem CreateFeedback(string name)
        {
            return new Feedback(name);
        }     
    }
}
