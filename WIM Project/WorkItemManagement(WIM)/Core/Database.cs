﻿using System;
using System.Collections.Generic;
using System.Text;
using WIMProject.Core.Contracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Core
{

    public class Database : IDatabase
    {
        private Database()
        {
        }
        private static IDatabase instance = null;
        public static IDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Database();
                }

                return instance;
            }
        }


        private  List<IPerson> person = new List<IPerson>();
        public IList<IPerson> Person
        {
            get => this.person;
        }

        //private List<IBug> bugs = new List<IBug>();
        //public IList<IBug> Bugs
        //{
        //    get => this.bugs;
        //}

        private readonly List<ITeam> teams = new List<ITeam>();
        public IList<ITeam> Teams
        {
            get => this.teams;
        }

        private readonly List<IWorkItem> workItems = new List<IWorkItem>();
        public IList<IWorkItem> WorkItems
        {
            get => this.workItems;
        }

        private readonly List<IBoard> boards = new List<IBoard>();
        public IList<IBoard> Boards
        {
            get => this.boards;
        }

        public void Clear()
        {
            this.teams.Clear();
            this.boards.Clear();
            this.workItems.Clear();
            this.person.Clear();
        }
    }
}

