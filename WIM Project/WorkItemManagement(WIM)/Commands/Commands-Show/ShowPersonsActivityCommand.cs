﻿using System.Linq;
using System.Collections.Generic;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;
using System.Text;
using System;
using System.Globalization;

namespace WIMProject.Commands
{
    public class ShowPersonsActivityCommand : Command
    {
        public ShowPersonsActivityCommand(IList<string> commandParameters)
            : base(commandParameters)
        {
        }
        public override string Execute()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            string dudeName = textInfo.ToTitleCase(CommandParameters[0]);

            StringBuilder sb = new StringBuilder();

            if (this.Database.Person.First(p => p.PersonName == dudeName).ActivityHistory.Count == 0)
                return $"{this.Database.Person.First(p => p.PersonName == dudeName)} has no activity history!";
            else
                foreach (var ev in this.Database.Person.First(p => p.PersonName == dudeName).ActivityHistory)
                {
                    sb.AppendLine();
                    sb.AppendLine($"* {ev}");
                }

            return $"{Environment.NewLine}{dudeName} has worked on:{Environment.NewLine}{sb.ToString().TrimEnd()}";
            
            
            // na sb мисля че трябва да има .ToString().TrimEnd()
            // Съгласих се :)
        }
    }
}
