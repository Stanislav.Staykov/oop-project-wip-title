﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class ShowBoardActivityCommand : Command
    {
        public ShowBoardActivityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            return this.Database.Boards.Count > 0 ?
            string.Join($"{Environment.NewLine}", this.Database.Boards.First(b => b.Name == textInfo.ToTitleCase(CommandParameters[0])).BoardHistory)
               : "There are no WorkItems in this board!";
        }
    }
}
