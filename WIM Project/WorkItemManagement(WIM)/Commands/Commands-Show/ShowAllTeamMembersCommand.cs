﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject;
using WIMProject.Commands.Abstracts;

namespace WIMProject.Commands
{
    public class ShowAllTeamMembersCommand : Command
    {
        public ShowAllTeamMembersCommand(IList<string> commandParameters)
            : base(commandParameters)
        {
        }
        public override string Execute()
        {
            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException("Invalid parameters count.");
            }

            var teamName = CommandParameters[0];
            if (!this.Database.Teams.Any(t => t.Name.Equals(teamName)))
            {
                throw new ArgumentException($"Team with name: {teamName}, does not exist!");
            }

            return this.Database.Teams.First(t => t.Name.Equals(teamName)).TeamMembers.Count > 0 ?
              string.Join($"{Environment.NewLine}",this.Database.Teams.First(t => t.Name == teamName).TeamMembers.Select(tm => tm.PersonName).ToList())
             : "There are no registered people.";
            // Raboti si super! :D 
        }
    }
}
