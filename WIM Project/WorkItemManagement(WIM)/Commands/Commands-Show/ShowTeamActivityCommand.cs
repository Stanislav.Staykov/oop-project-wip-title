﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class ShowTeamActivityCommand : Command
    {
        public ShowTeamActivityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute() 
        { 
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

        ITeam teamToPrint = this.Database.Teams.First(t => t.Name == textInfo.ToTitleCase(CommandParameters[0]));

        StringBuilder sb = new StringBuilder();

        sb.AppendLine();

            foreach(var person in teamToPrint.TeamMembers)
            {
                sb.Append(new ShowPersonsActivityCommand(new List<string> {person.PersonName}).Execute());
            }

        return sb.ToString().Trim();
        }
    }
}
