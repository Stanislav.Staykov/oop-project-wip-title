﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using WIMProject.Commands.Abstracts;

namespace WIMProject.Commands
{
    public class ShowAllPeopleCommand : Command
    {
        public ShowAllPeopleCommand(IList<string> commandParameters)
            : base(commandParameters)
        {       
        }

        public override string Execute()
        {

            return this.Database.Person.Count > 0 ?
                string.Join($"{Environment.NewLine}", this.Database.Person.Select(p => p.PersonName).ToList())
               : "There are no registered people.";
        }
    }
}
