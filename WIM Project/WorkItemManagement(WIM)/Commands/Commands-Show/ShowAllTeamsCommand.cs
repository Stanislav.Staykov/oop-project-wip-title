﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;

namespace WIMProject.Commands
{
    public class ShowAllTeamsCommand : Command
    {
        public ShowAllTeamsCommand(IList<string> commandParameters)
            : base(commandParameters)
        {

        }
        public override string Execute()
        {
           if(this.Database.Teams.Count == 0)
                return "There are no registered teams.";

            StringBuilder sb = new StringBuilder();

            foreach(var team in this.Database.Teams)
            {
                sb.Append("* ");
                sb.AppendLine(team.Name);
            }
            return sb.ToString();
        }
    }
}
