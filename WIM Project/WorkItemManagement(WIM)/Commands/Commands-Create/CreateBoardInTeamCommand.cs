﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject;
using WIMProject.Commands.Abstracts;

namespace WIMProject.Commands
{
    public class CreateBoardInTeamCommand : Command
    {
        public CreateBoardInTeamCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            string boardName;
            string teamName;
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            try
            {
                boardName = textInfo.ToTitleCase(CommandParameters[0].Trim());
                teamName = textInfo.ToTitleCase(CommandParameters[1].Trim());
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateBoard command parameters.");
            }

            if (!this.Database.Teams.Any(t => t.Name.Equals(teamName)))
                throw new ArgumentException("Team with this name does not exist!");

            if (this.Database.Teams.First(t => t.Name.Equals(teamName)).TeamBoards.Any(bN => bN.Name == boardName))
                throw new ArgumentException("Board with this name already exists in this team!");

            var teamIndex = this.Database.Teams.IndexOf(this.Database.Teams.First(t => t.Name.Equals(teamName)));
            
            this.Database.Teams[teamIndex].TeamBoards.Add(new Board(boardName));
            this.Database.Boards.Add(new Board(boardName));
            this.Database.Boards.Last().AddTeam(teamName);
                

            return $"Board {boardName} was created in Team {teamName}";

        }
    }
}
