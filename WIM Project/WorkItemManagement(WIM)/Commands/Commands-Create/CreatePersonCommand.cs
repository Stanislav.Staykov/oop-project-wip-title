﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;
using System.Globalization;

namespace WIMProject.Commands
{
    public class CreatePersonCommand : Command
    {
        public CreatePersonCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException("Invalid parameters count.");
            }

            //Тука с това текстИнфо оправяме името да влиза винаги с главни букви;
            //Мислиш ли, че можем да го включим някак си в Command? За да може всичкият ни инпут да изглежда добре 
            //и да можем да четем разлики в кейса (като сме направили Иван Иванов, да можем да го намерим като иван иванов)
            // дааааа с string.tolower и ги записваме в базата с малки букви

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            string name = textInfo.ToTitleCase(CommandParameters[0].Trim());

            if (this.Database.Person.Any(p => p.PersonName == name))
                throw new ArgumentException($"Person with name {name} already exists.");

            IPerson person = this.Factory.CreatePerson(name);
            this.Database.Person.Add(person);

            return $"Person {this.Database.Person.Last().PersonName} was created";
        }
    }
}