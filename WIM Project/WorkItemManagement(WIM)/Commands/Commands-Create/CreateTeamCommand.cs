﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class CreateTeamCommand : Command
    {

        public CreateTeamCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            string teamName;
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            try
            {
                teamName = textInfo.ToTitleCase(CommandParameters[0].Trim());
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateTeam command parameters.");
            }

            if (this.Database.Teams.Any(t => t.Name.Equals(teamName)))
                throw new ArgumentException("Team with this name already exists!");

            ITeam team = this.Factory.CreateTeam(teamName);
            this.Database.Teams.Add(team);

            return $"Team {this.Database.Teams.Last().Name} was created";
        }
    }
}
