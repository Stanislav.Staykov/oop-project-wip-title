﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class CreateWorkItemCommand : Command
    {
        public CreateWorkItemCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            string workItemName;
            string workItemBoard;

            IList<string> stringList = this.CommandParameters[1].Split(',').ToList();

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            try
            {
                workItemName = textInfo.ToTitleCase(stringList[0].Trim());
                workItemBoard = textInfo.ToTitleCase(stringList[1].Trim());

            }
            catch
            {
                throw new ArgumentException("Failed to parse WorkItem command parameters.");
            }

            //Тук проверяваме дали съществува дъската;

            if (!this.Database.Teams.Any(t => t.TeamBoards.Any(tb => tb.Name == workItemBoard)))
                throw new ArgumentException("Board with this name does not exist! Please provide a valid board!");

            switch (this.CommandParameters[0])
            {
                case
                    "create bug":
                    {

                        //До колкото аз разбираам, всяка инстанция Bug наследява IBug, а ние викаме инстанция и то GetType().Name гледа нейното име, а не името на баща и :D

                        if (this.Database.WorkItems.Any(b => b.Title == workItemName && b.GetType().Name == "Bug"))
                            throw new ArgumentException($"Bug with this title already exists!");

                        IBug bug = (IBug)this.Factory.CreateBug(workItemName);

                        this.Database.WorkItems.Add(bug);
                        this.Database.WorkItems.Last().AddBoard(workItemBoard);

                        //Tова тук е ново - тук вкарваме в BoardActivity-то, че в борда е създаден уъркайтем. 
                        this.Database.Boards.First(b => b.Name == workItemBoard).AddEvent($"{Environment.NewLine}Bug {this.Database.WorkItems.Last().Title} " +
                            $"with ID: {this.Database.WorkItems.Last().ID} was created on {DateTime.Now:dd/MM/yyyy} at {DateTime.Now:hh:MM ZZ}" +
                            $"{Environment.NewLine}");

                        return $"{Environment.NewLine}Bug {this.Database.WorkItems.Last().Title} with ID: {this.Database.WorkItems.Last().ID} was created. " +
                            $"{Environment.NewLine}" +
                            $"{Environment.NewLine}Please, describe the problem, starting your explanation with 'add description:'";
                    }
                case
                    "create story":
                    {
                        if (this.Database.WorkItems.Any(s => s.Title == workItemName && s.GetType().Name == "Story"))
                            throw new ArgumentException($"Story with this title already exists!");

                        IStory story = (IStory)this.Factory.CreateStory(workItemName);

                        this.Database.WorkItems.Add(story);
                        this.Database.WorkItems.Last().AddBoard(workItemBoard);
                        
                        this.Database.Boards.First(b => b.Name == workItemBoard).AddEvent($"{Environment.NewLine}Story {this.Database.WorkItems.Last().Title} " +
                            $"with ID: {this.Database.WorkItems.Last().ID} was created on {DateTime.Now:dd/MM/yyyy} at {DateTime.Now:hh:MM ZZ}" +
                            $"{Environment.NewLine}");


                        return $"{Environment.NewLine}Story {this.Database.WorkItems.Last().Title} with ID: {this.Database.WorkItems.Last().ID} was created. " +
                            $"{Environment.NewLine}" +
                            $"{Environment.NewLine}Please, describe your case starting the description with 'add description:' or continue within the program.";
                    }
                case
                    "create feedback":
                    {
                        if (this.Database.WorkItems.Any(f => f.Title == workItemName && f.GetType().Name == "Feedback"))
                            throw new ArgumentException($"Feedback with this title already exists!");

                        IFeedback feedback = (IFeedback)this.Factory.CreateFeedback(workItemName);

                        this.Database.WorkItems.Add(feedback);
                        this.Database.WorkItems.Last().AddBoard(workItemBoard);
                        this.Database.Boards.First(b => b.Name == workItemBoard).AddEvent($"{Environment.NewLine}Feedback {this.Database.WorkItems.Last().Title} " +
                            $"with ID: {this.Database.WorkItems.Last().ID} was created on {DateTime.Now:dd/MM/yyyy} at {DateTime.Now:hh:MM ZZ}" +
                            $"{Environment.NewLine}");


                        return $"{Environment.NewLine}Feedback {this.Database.WorkItems.Last().Title} with ID: {this.Database.WorkItems.Last().ID} was created. " +
                            $"{Environment.NewLine}" +
                            $"{Environment.NewLine}Please, describe your experience starting the description with 'add description:' and rate it with 'add rating:' or continue within the program";
                    }
                default:
                    return $"{Environment.NewLine}Could not parse CreateWorkItemCommand Parameters!";
            }
        }
    }
}
