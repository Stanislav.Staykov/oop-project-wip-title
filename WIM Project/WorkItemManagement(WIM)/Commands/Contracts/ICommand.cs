﻿
namespace WIMProject.Commands.Contracts
{
    public interface ICommand
    {
        string Execute();
    }
}
