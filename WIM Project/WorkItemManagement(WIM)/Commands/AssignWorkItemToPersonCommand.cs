﻿using System;
using System.Collections.Generic;
using System.Linq;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;
using WIMProject;
using System.Globalization;

namespace WIMProject.Commands
{
    public class AssignWorkItemToPersonCommand : Command
    {
        public AssignWorkItemToPersonCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;//?


            if (CommandParameters.Count < 2)
                throw new ArgumentException("Please enter valid parameters!");

            if (!int.TryParse(CommandParameters[0].Trim(), out int workItemId))
                throw new ArgumentException("ID must be a number!");

            if (!this.Database.WorkItems.Any(wI => wI.ID == workItemId))
                throw new ArgumentException("ID does not exist! Please provide a valid ID.");

            IPerson personToAssign = this.Database.Person.First(p => p.PersonName == textInfo.ToTitleCase(CommandParameters[1].Trim()));//?

            if (this.Database.Person.First(n => n.PersonName == personToAssign.PersonName).WorkItems.Any(wI=>wI.ID == workItemId))
                throw new ArgumentException("Person is already working on this item! Please provide another item!");

            this.Database.WorkItems.First(wI => wI.ID == workItemId).AssignPerson(personToAssign);
            this.Database.Person.First(p => p == personToAssign).AddWorkItem(this.Database.WorkItems.First(wI => wI.ID == workItemId));

            this.Database.Person.First(p => p.PersonName == personToAssign.PersonName).AddEvent($"{personToAssign.PersonName} " +
                                                              $"was assigned {this.Database.WorkItems.First(wI => wI.ID == workItemId).GetType().Name}" +
                                                              $" with ID {workItemId}");

            this.Database.Boards.First(b => b.Name == this.Database.WorkItems.First(wI => wI.ID == workItemId).Board).AddEvent($"{personToAssign.PersonName} " +
                                                              $"was assigned {this.Database.WorkItems.First(wI => wI.ID == workItemId).GetType().Name}" +
                                                              $" with ID {workItemId} in board {this.Database.Boards.First(b => b.Name == this.Database.WorkItems.First(wI => wI.ID == workItemId).Board)}" +
                                                              $"on {DateTime.Now:dddd dd/MM/yyyy hh:mm tt}");

            return $"{this.Database.WorkItems.First(wI => wI.ID == workItemId).GetType().Name} " +
                $"with ID {workItemId} was assigned to {personToAssign.PersonName}";
        }
    }
}
