﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    class ListAllOfTypeCommand : Command
    {
        public ListAllOfTypeCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            string optionSelector;

            optionSelector = textInfo.ToTitleCase(CommandParameters[1]);

            return string.Join($"{Environment.NewLine}", this.Database.WorkItems.Where(wI => wI.GetType().Name == textInfo.ToTitleCase(optionSelector)).Select(wI => wI.Title).ToList());
        }

        public IList<IWorkItem> ToList()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            string optionSelector;

            optionSelector = textInfo.ToTitleCase(CommandParameters[1]);

            IList<IWorkItem> workItemList = this.Database.WorkItems.Where(wI => wI.GetType().Name == textInfo.ToTitleCase(optionSelector)).ToList();

            return workItemList;
        }
    }
}
