﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class ListAllCommand : Command
    {
        public ListAllCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            if (CommandParameters.Count == 1 && this.Database.WorkItems.Count > 0)
                return string.Join($"{Environment.NewLine}", this.Database.WorkItems.Select(wI => wI.Title).ToList());

            if(CommandParameters.Count == 2 && this.Database.WorkItems.Count > 0)
            {
                return new ListAllOfTypeCommand(this.CommandParameters).Execute();
            }

            if (CommandParameters.Count == 3 && CommandParameters[1].TrimStart() == "sorted by")
            {
                return new ListAllSortedByCommand(CommandParameters).Execute();
            }

            if (CommandParameters.Count == 3 || CommandParameters.Count == 4)
            {
                return new ListAllOfStatusOrAssigneeCommand(this.CommandParameters).Execute();
            }

            //list all: sorted by: 

            return "Please input valid parameters!";
        }
        public IList<IWorkItem> ToList()
        {

            IList<IWorkItem> workItemList = this.Database.WorkItems;
            return workItemList;
        }

    }
}
