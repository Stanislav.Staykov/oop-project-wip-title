﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class ListAllSortedByCommand : Command
    {
        public ListAllSortedByCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            string filterSelector = CommandParameters[2];
            switch (filterSelector.ToLower())
            {
                case "title":
                    return string.Join($"{Environment.NewLine}", this.Database.WorkItems.OrderBy(wI => wI.Title).Select(wI => wI.Title));
                case "priority":
                    IList<IWorkItem> bugsForPriority = new ListAllOfTypeCommand(new List<string> {"list all","Bug"}).ToList();
                    IList<IWorkItem> storiesForPriority = new ListAllOfTypeCommand(new List<string> { "list all", "Story" }).ToList();
                    IList<IStory> trueStories = new List<IStory>();
                    IList<IBug> trueBugs = new List<IBug>();

                    foreach(var bug in bugsForPriority)
                    {
                        trueBugs.Add((IBug)bug);
                    }

                    foreach(var story in storiesForPriority)
                    {
                        trueStories.Add((IStory)story);
                    }

                    trueBugs = trueBugs.OrderBy(bug => bug.BugPriorityType).ToList();
                    trueStories = trueStories.OrderBy(story => story.PriorityType).ToList();

                    return string.Join($"{Environment.NewLine}", trueBugs.Select(tb => tb.Title).Concat(trueStories.Select(ts => ts.Title)));

                case "severity":
                    IList<IBug> trueBugsForSever = new List<IBug>();
                    IList<IWorkItem> restOfJobs = new ListAllCommand(new List<string> {"list all"}).ToList();

                    foreach (var bug in new ListAllOfTypeCommand(new List<string> { "list all", "Bug" }).ToList())
                    {
                        trueBugsForSever.Add((IBug)bug);
                        restOfJobs.Remove(bug);
                    }

                    restOfJobs = restOfJobs.OrderBy(job => job.Title).ToList();
                    trueBugsForSever = trueBugsForSever.OrderBy(bug => bug.BugSeverityType).ToList();

                    return string.Join($"{Environment.NewLine}", trueBugsForSever.Select(tbs => tbs.Title).Concat(restOfJobs.Select(roj => roj.Title)));

                case "size":
                    IList<IStory> trueStoryForSize = new List<IStory>();
                    IList<IWorkItem> nonStories = new ListAllCommand(new List<string> { "list all" }).ToList();

                    foreach (var story in new ListAllOfTypeCommand(new List<string> { "list all", "Story" }).ToList())
                    {
                        trueStoryForSize.Add((IStory)story);
                        nonStories.Remove(story);
                    }

                    trueStoryForSize=trueStoryForSize.OrderBy(job => job.SizeType).ToList();
                    nonStories=nonStories.OrderBy(bug => bug.Title).ToList();

                    return string.Join($"{Environment.NewLine}", trueStoryForSize.Select(tbs => tbs.Title).Concat(nonStories.Select(roj => roj.Title)));

                case "rating":
                    IList<IFeedback> trueFeedback = new List<IFeedback>();
                    IList<IWorkItem> nonFeedback = new ListAllCommand(new List<string> { "list all" }).ToList();

                    foreach (var feed in new ListAllOfTypeCommand(new List<string> { "list all", "Feedback" }).ToList())
                    {
                        trueFeedback.Add((IFeedback)feed);
                        nonFeedback.Remove(feed);
                    }

                    trueFeedback = trueFeedback.OrderBy(feed => feed.Rating).ToList();
                    nonFeedback = nonFeedback.OrderBy(bug => bug.Title).ToList();

                    return string.Join($"{Environment.NewLine}", trueFeedback.Select(tbs => tbs.Title).Concat(nonFeedback.Select(roj => roj.Title)));
                default:
                    return "Please enter valid parameters for sorting!";
            }
        }
    }
}
