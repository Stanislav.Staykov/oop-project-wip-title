﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    class ListAllOfStatusOrAssigneeCommand : Command
    {
        public ListAllOfStatusOrAssigneeCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            string newCommand = string.Join(":", CommandParameters);
            IList<string> newCommandParameters = newCommand.Split(new char[] { ':', ',' }, StringSplitOptions.RemoveEmptyEntries);

            string typeSelector = newCommandParameters[1];

            //list all: bug, status: neshto si, assignee: nqkoi si

            //list all:assigned to:Petar

            switch (typeSelector.ToLower())
            {
                case "assigned to":

                    return string.Join($"{Environment.NewLine}", this.Database.WorkItems.Where(wI => wI.Assignee != null && wI.Assignee.PersonName == textInfo.ToTitleCase(CommandParameters[2])).Select(wI => wI.Title));

                case "bug":

                    IList<IBug> bugList = new List<IBug>();

                    foreach (var bug in this.Database.WorkItems.Where(wI => wI.GetType().Name == "Bug"))
                    {
                        bugList.Add(bug as Bug);
                    }

                    Enum.TryParse(newCommandParameters[3], true, out BugStatusType bugStatus); //тези ще са според опциите на конкретния вид УъркАйтем
                    Enum.TryParse(newCommandParameters[3], true, out PriorityType bugPrio);

                    if (newCommandParameters[2] == "status")
                        return string.Join($"{Environment.NewLine}", bugList.Where(bug => bug.BugStatusType == bugStatus).Select(bug => bug.Title));

                    if (newCommandParameters[2] == "assignee")
                        return string.Join($"{Environment.NewLine}", bugList.Where(bug => bug.Assignee != null && bug.Assignee.PersonName == newCommandParameters[3]).Select(bug => bug.Title));

                    //if (newCommandParameters[2] == "priority")
                    //    return string.Join($"{Environment.NewLine}", bugList.Where(bug => bug.Assignee != null && bug.BugPriorityType == bugPrio).Select(bug => bug.Title));

                    else if (newCommandParameters.Count == 6)
                        return string.Join($"{Environment.NewLine}", bugList.Where(bug => bug.BugStatusType == bugStatus && bug.Assignee.PersonName == newCommandParameters[5]));

                    else
                        return "Please enter valid parameters!";

               
                case "story":
                    //list all: story, status: neshto si, assignee: nqkoi si

                    IList<IStory> storyList = new List<IStory>();

                    foreach (var story in this.Database.WorkItems.Where(wI => wI.GetType().Name == "Story"))
                    {
                        storyList.Add(story as Story);
                    }


                    if (newCommandParameters.Count == 4 && newCommandParameters[2] == "status")
                    {
                        Enum.TryParse(newCommandParameters[3], true, out StoryStatusType storyStatus);
                        return string.Join($"{Environment.NewLine}", storyList.Where(story => story.StoryStatusType == storyStatus).Select(story => story.Title));
                    }

                    if (newCommandParameters.Count == 4 && newCommandParameters[2] == "assignee")// 2- asignee
                        return string.Join($"{Environment.NewLine}", storyList.Where(story => story.Assignee != null && story.Assignee.PersonName == newCommandParameters[3]).Select(story => story.Title));// PersonName == newCommandParameters[3])

                   

                    else if (newCommandParameters.Count == 6)
                    {
                        Enum.TryParse(newCommandParameters[3], true, out StoryStatusType storyStatus);
                        return string.Join($"{Environment.NewLine}", storyList.Where(story => story.StoryStatusType == storyStatus && story.Assignee.PersonName == newCommandParameters[5]).Select(story => story.Title));
                    }

                    else
                        return "Please enter valid parameters!";
                    //---------------------------------------------------------------------
                                        //create team: team
                                        //create board: board,team
                                        //create person: petar
                                        //create bug: bug_000000000000000001,board
                                        //create bug: bug_000000000000000002,board
                                        //create story: story_000000000000000001, board
                                        //create story: story_000000000000000002, board
                                        //create feedback: feed_00000000000000001,board
                                        //create feedback: feed_00000000000000002,board
                                        //change bug option: 1,priority,High
                                        //change bug option: 2,status,Fixed
                                        //change story option: 3,status,Done
                                        //assign work: 1,petar
                                        //assign work: 3,petar
                                        //assign work: 6,petar
                    //------------------------------------------------------------------------

                case "feedback":
                    IList<IFeedback> feedbackList = new List<IFeedback>();
                    foreach (var feedback in this.Database.WorkItems.Where(wI => wI.GetType().Name == "Feedback"))
                    {
                        feedbackList.Add(feedback as Feedback);
                    }

                    Enum.TryParse(newCommandParameters[3], true, out FeedbackStatusType feedbackStatus);

                    if (newCommandParameters.Count == 4 && newCommandParameters[2] == "status")
                        return string.Join($"{Environment.NewLine}", feedbackList.Where(feed => feed.FeedbackStatusType == feedbackStatus).Select(feed => feed.Title));

                    if (newCommandParameters.Count == 4 && newCommandParameters[2] == "assignee")
                        return string.Join($"{Environment.NewLine}", feedbackList.Where(feed => feed.Assignee!=null &&  feed.Assignee.PersonName == newCommandParameters[3]).Select(feed => feed.Title));

                    else if (newCommandParameters.Count == 6)
                        return string.Join($"{Environment.NewLine}", feedbackList.Where(feed =>  feed.Assignee!=null && feed.FeedbackStatusType == feedbackStatus && feed.Assignee.PersonName == newCommandParameters[5]).Select(feed => feed.Title));
                    
                 

                    else
                        return "Please enter valid parameters!";
                default:
                    return "Please enter valid parameters";

            }

            //neshto si: pa

            //list all: bugs, sorted by: title/priority/severity/size/rating


            //return string.Join($"{Environment.NewLine}", this.Database.WorkItems.Where(wI => wI.GetType().Name == textInfo.ToTitleCase(optionSelector)).Select(wI => wI.Title).ToList());


        }
    }
}
