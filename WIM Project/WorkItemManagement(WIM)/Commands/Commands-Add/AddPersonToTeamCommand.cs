﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class AddPersonToTeamCommand : Command
    {
        public AddPersonToTeamCommand(IList<string> commandParameters)
            : base(commandParameters)
        {

        }
        public override string Execute()
        { 
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            var personName = textInfo.ToTitleCase(CommandParameters[0].Trim());
            var teamName = textInfo.ToTitleCase(CommandParameters[1].Trim());

            if (!this.Database.Person.Any(p => p.PersonName.Equals(personName)))
                throw new ArgumentException($"Person with name: {personName} does not exists.");

            if (!this.Database.Teams.Any(t => t.Name==teamName))
                throw new ArgumentException($"Team with name: {teamName} does not exists.");

            if (this.Database.Teams.First(t => t.Name == teamName).TeamMembers.Any(tM => tM.PersonName == personName))
                throw new ArgumentException($"{personName} is already part of this team!");

           //TODO: Работи си чудесно! :) 
            ITeam currentTeam = this.Database.Teams.First(t => t.Name.Equals(teamName));
            IPerson currentPerson = this.Database.Person.First(p => p.PersonName.Equals(personName));


            currentTeam.TeamMembers.Add(currentPerson);
            return $"{personName} was added to team {teamName}.";
        }
    }
}
