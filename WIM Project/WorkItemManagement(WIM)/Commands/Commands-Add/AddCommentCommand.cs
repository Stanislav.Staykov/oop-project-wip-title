﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Core;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class AddCommentCommand : Command
    {
        public AddCommentCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }



        public override string Execute()
        {
            if (!int.TryParse(CommandParameters[0], out int itemId))
            {
                throw new ArgumentException("ID must be a number!");
            }

            var comment = CommandParameters.Skip(1).ToString();

            if (!Database.WorkItems.Any(w => w.ID==itemId))
            {
                throw new ArgumentException($"Item with ID: {itemId} does not exist.");
            }

            IWorkItem workItem = Database.WorkItems.First(w => w.ID==itemId);
            workItem.AddComment(comment);

            return $"Comment added to {workItem.GetType().Name} with ID {itemId}";
        }
    }
}
