﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class AddDescriptionCommand : Command
    {
        public AddDescriptionCommand(IList<string> commandParameters)
            :base(commandParameters)
        {
        }


        //add description: neshto si
        //add description: 1, neshto si
        public override string Execute()
        {
            if(CommandParameters.Count == 1)
            {
            var description = string.Join($"{Environment.NewLine}", CommandParameters[0]);           
            
            if (description.Length < 10 || description.Length > 500)
            {
                throw new ArgumentException("The description must be at least 10, and less than 500 characters!");
            }


            this.Database.WorkItems.Last().AddDescription(description);

            return $"Description added to {this.Database.WorkItems.Last().GetType().Name} with ID {this.Database.WorkItems.Last().ID}";
            }

            else if (CommandParameters.Count == 2)
            {
                var workItemId = int.Parse(CommandParameters[0]);
                var description = string.Join($"{Environment.NewLine}", CommandParameters[1]);

                if (description.Length < 10 || description.Length > 500)
                {
                    throw new ArgumentException("The description must be at least 10, and less than 500 characters!");
                }

                this.Database.WorkItems.First(wI => wI.ID == workItemId).AddDescription(description);

                return $"Description added to {this.Database.WorkItems.First(wI => wI.ID == workItemId).GetType().Name} with ID {this.Database.WorkItems.First(wI => wI.ID == workItemId).ID}";
            }

            return "Please make sure you have provided a correct ID and a description, separated by a comma and preceded by the 'add description' command";

        }
    }
}
