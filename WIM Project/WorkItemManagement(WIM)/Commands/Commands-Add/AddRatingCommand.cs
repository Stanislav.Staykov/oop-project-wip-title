﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class AddRatingCommand : Command
    {
        public AddRatingCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            if (!int.TryParse(CommandParameters[0], out int itemId))
            {
                throw new ArgumentException("ID must be a number!");
            }

            if(this.Database.WorkItems.First(wI => wI.ID == itemId).GetType().Name.ToLower() !="feedback")
            {
                throw new ArgumentException($"WorkItem with ID {itemId} is not Feedback.");
            }

            int.TryParse(CommandParameters[1], out int rating);
            if(rating>0 && rating < 5)
            {
                IWorkItem preFeed = this.Database.WorkItems.First(wI => wI.ID == itemId);
                IFeedback feed = (IFeedback)preFeed;
                feed.ChangeRating(rating);

                return "Than you for rating our services!";
            }
            else
            {
                throw new ArgumentException("Please, provide valid parameters");
            }

        }
    }
}
