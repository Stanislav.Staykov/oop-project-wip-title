﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class ChangeFeedbackOptionCommand : Command
    {
        public ChangeFeedbackOptionCommand(IList<string> commandParameters) : base(commandParameters)
        {

        }       
        
        public override string Execute()
        {
            if (this.CommandParameters.Count < 3)
                throw new ArgumentException("Invalid parameters count.");

            if (!int.TryParse(CommandParameters[0], out int workItemId))            
                throw new ArgumentException($"ID it is not number.");

            if (!this.Database.WorkItems.Any(wi => wi.ID == workItemId  && wi.GetType().Name == "Feedback"))// Feedback or IFeedback
                throw new ArgumentException($"Feedback with ID: {workItemId}, does not exist!");
            
            var currentFeedback = this.Database.WorkItems.First(wi => wi.ID == workItemId);
            
            switch (CommandParameters[1].ToLower())
            { 
                case "rating":
                    {
                        if (!int.TryParse(CommandParameters[2], out int rating))
                            throw new ArgumentException($"{CommandParameters[2]} must be a number!");

                        (currentFeedback as IFeedback).ChangeRating(rating);
                        
                        return $"The Feedback's Rating was changed to " +
                              $"{(currentFeedback as IFeedback).Rating}";
                    }
                case "status":
                    {
                        if (!Enum.TryParse<FeedbackStatusType>(CommandParameters[2].ToLower(), true, out FeedbackStatusType feedbackStatusType))
                            throw new ArgumentException($"{CommandParameters[2]} it's not a part of FeedbackStatusType!");

                        (currentFeedback as IFeedback).ChangeStatus(feedbackStatusType);

                        return $"The Feedback's Status was changed to " +
                              $"{(currentFeedback as IFeedback).FeedbackStatusType}";
                    }
                default:
                    return "Please provide a valid property to change!";
            }

        }
    }
}
