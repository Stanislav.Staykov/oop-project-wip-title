﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class ChangeStoryOptionCommand : Command
    {
        public ChangeStoryOptionCommand(IList<string> commandParameters)
           : base(commandParameters)
        {

        }

        public override string Execute()
        {
            if (CommandParameters.Count < 3)
            {
                throw new ArgumentException("Invalid parameters count.");
            }

            if (!int.TryParse(CommandParameters[0], out int currentStoryId))
            {
                throw new ArgumentException("ID must be a number!");
            }

            var currentStory = this.Database.WorkItems.First(s => s.ID == currentStoryId);// май ще трябва List<IBug, IStory, IFeedback> in database
            var type = currentStory.GetType().Name;

            if (!(type == "Story"))
            {
                throw new ArgumentException($"The item with ID: {currentStoryId} is not a Story!");
            }

            var currentCommand = CommandParameters[1].ToLower();
            var setNewOption = CommandParameters[2].ToLower();

            switch (currentCommand)
            {
                case "priority":
                    {
                        if (!Enum.TryParse<PriorityType>(setNewOption, true, out PriorityType priorityType))
                        {
                            throw new ArgumentException("Invalid priority type");
                        }
                        //this.Database.WorkItems.First(b => b.ID.Equals(currentBugId)).
                        (currentStory as IStory).ChangePriority(priorityType);
                       
                        return $"The Story's Priority was changed to " +
                               $"{(currentStory as IStory).PriorityType}";

                    }
                case "size":
                    {
                        if (!Enum.TryParse<StorySizeType>(setNewOption, true, out StorySizeType storySizeType))
                        {
                            throw new ArgumentException("Invalid Size type");
                        }
                        //this.Database.WorkItems.First(b => b.ID.Equals(currentBugId)).
                        (currentStory as IStory).ChangeSizeType(storySizeType);
                        
                        return $"The Story's Size was changed to " +
                               $"{(currentStory as IStory).SizeType}";
                    }
                case "status":
                    {
                        if (!Enum.TryParse<StoryStatusType>(setNewOption, true, out StoryStatusType storyStatusType))
                        {
                            throw new ArgumentException("Invalid status type");
                        }
                        //this.Database.WorkItems.First(b => b.ID.Equals(currentBugId)).
                        (currentStory as IStory).ChangeStatusType(storyStatusType);
                        
                        return $"The Story's Status was changed to " +
                            $"{(currentStory as IStory).StoryStatusType}";
                    }
                default:
                    return "Please provide a valid property to change!";
            }
        }
    }
}
