﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class ChangeBugOptionCommand : Command
    {
        public ChangeBugOptionCommand(IList<string> commandParameters)
             : base(commandParameters)
        {

        }
        public override string Execute()
        {
            if (CommandParameters.Count < 3)
            {
                throw new ArgumentException("Invalid parameters count.");
            }

            if (!int.TryParse(CommandParameters[0], out int currentBugId))
            {
                throw new ArgumentException("It is not number.");
            }



            var currentBug = this.Database.WorkItems.First(b => b.ID == currentBugId);
            // май ще трябва List<IBug, IStory, IFeedback> in database
            var type = currentBug.GetType().Name;
            if (!(type == "Bug"))
            {
                throw new ArgumentException($"The item with ID: {currentBugId} is not a Bug!");//???????????????????????????

            }

            var currentCommand = CommandParameters[1].ToLower();
            var setNewOption = CommandParameters[2].ToLower();

            switch (currentCommand)
            {
                case "priority":
                    {
                        if (!Enum.TryParse<PriorityType>(setNewOption, true, out PriorityType priorityType))
                        {
                            throw new ArgumentException("Invalid priority type");
                        }
                        //this.Database.WorkItems.First(b => b.ID.Equals(currentBugId)).
                        (currentBug as IBug).ChangePriority(priorityType);
                        
                        return $"The Bug's Priority was changed to " +
                               $"{(currentBug as IBug).BugPriorityType}";

                    }
                case "severity":
                    {
                        if (!Enum.TryParse<BugSeverityType>(setNewOption, true, out BugSeverityType bugSeverityType))//тук май тряжва да имаоще нещо след трай парс  var parsed = Enum.TryParse<Initiative>(initiative, out parsedInitiativeAsEnum); ама май не
                        {
                            throw new ArgumentException("Invalid severity type");
                        }
                        //this.Database.WorkItems.First(b => b.ID.Equals(currentBugId)).
                        (currentBug as IBug).ChangeSeverity(bugSeverityType);
                        
                        return $"The Bug's Severity was changed to " +
                               $"{(currentBug as IBug).BugSeverityType}";
                    }
                case "status":
                    {
                        if (!Enum.TryParse<BugStatusType>(setNewOption, true, out BugStatusType bugStatusType))
                        {
                            throw new ArgumentException("Invalid status type");
                        }
                        //this.Database.WorkItems.First(b => b.ID.Equals(currentBugId)).                        
                        (currentBug as IBug).ChangeStatus(bugStatusType);

                        return $"The Bug's Status was changed to " +
                            $"{(currentBug as IBug).BugStatusType}";
                    }
                default:
                    return "Please provide a valid property to change!";
            }

        }
    }
}
