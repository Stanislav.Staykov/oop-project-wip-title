﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Commands
{
    public class UnassignWorkItemToPersonCommand : Command
    {
        public UnassignWorkItemToPersonCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;//?

            if (CommandParameters.Count < 2)
                throw new ArgumentException("Please enter valid parameters!");

            IPerson personToUnassign = this.Database.Person.First(p => p.PersonName == textInfo.ToTitleCase(CommandParameters[1].Trim()));//?            

            if (!int.TryParse(CommandParameters[0].Trim(), out int workItemId))
                throw new ArgumentException("ID must be a number!");

            if (!this.Database.WorkItems.Any(wI => wI.ID == workItemId))
                throw new ArgumentException("ID does not exist! Please provide valid ID.");

            this.Database.WorkItems.First(wI => wI.ID == workItemId).UnassignPerson(personToUnassign);

            string typeString = this.Database.WorkItems.First(wI => wI.ID == workItemId).GetType().Name;

            this.Database.Person.First(p => p.PersonName == personToUnassign.PersonName).AddEvent($"{personToUnassign.PersonName} " +
                                                              $"was unassigned from {typeString}" +
                                                              $" with ID {workItemId}");
            return $"{typeString} " +
                $"with ID {workItemId} was unassigned from {personToUnassign.PersonName}";
        }
    }
}
