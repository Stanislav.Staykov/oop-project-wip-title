﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMProject.Models;

namespace WIMProject.Tests.Models.Tests.PersonTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CorrectlyAssignPassedValues()
        {
            //Arrange
            string name = new string('s', 11);

            //Act
            var person = new Person(name);

            //Assert
            Assert.AreEqual(name, person.PersonName);
        }
    }
}
