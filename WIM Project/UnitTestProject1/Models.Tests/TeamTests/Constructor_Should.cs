﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.TeamTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CorrectlyAssignPassedValues()
        {
            //Arrange
            string name = new string('s', 11);

            //Act
            var team = new Team(name);

            //Assert
            Assert.AreEqual(name, team.Name);

        }
    }
}
