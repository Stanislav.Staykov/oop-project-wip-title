﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.FeedbackTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CorrectlyAssignPassedValues()
        {
            //Arrange
            string nameFeedback = new string('S', 11);

            //Act
            var feedback = new Feedback(nameFeedback);

            //Assert
            Assert.AreEqual(nameFeedback, feedback.Title);

        }
    }
}
