﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.StoryTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CorrectlyAssignPassedValues()
        {
            //Arrange
            string nameStory = new string('S', 11);

            //Act
            var story = new Story(nameStory);

            //Assert
            Assert.AreEqual(nameStory, story.Title);

        }
    }
}
