﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.AbstractionWorkItem.Tests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CorrectlyAssignPassedValues()
        {
            //Arrange
            string nameBug = new string('S', 11);
            string nameFeedback = new string('A', 21);

            //Act
            var bug = new Bug(nameBug);
            var feedback = new Feedback(nameFeedback);

            //Assert
            Assert.AreEqual(nameBug, bug.Title);
            Assert.AreEqual(nameFeedback, feedback.Title);
        }
    }
}
