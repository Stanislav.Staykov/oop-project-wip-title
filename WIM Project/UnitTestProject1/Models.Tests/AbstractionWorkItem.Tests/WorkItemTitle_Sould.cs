﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.AbstractionWorkItem.Tests
{
    [TestClass]
    public class WorkItemTitle_Sould
    {      
        [TestMethod]
        public void ThrowWhenPassedValueIsLessThanMin()
        {
            //Arrange
            string nameBug = new string('s', 3);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Bug(nameBug));          
        }
        [TestMethod]
        public void ThrowWhenPassedValueIsLessThanMax()
        {
            //Arrange
            string nameBug = new string('s', 333);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Bug(nameBug));
        }

        [TestMethod]
        public void ThrowWhenPassedValueIsEmpty()
        {
            //Arrange
            string feedbackName = "";

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Feedback(feedbackName));
        }
        //[TestMethod]// DOTO- не тръгва
        //public void ThrowWhenPassedValueIsNull()
        //{
        //    //Arrange
        //    string feedbackName = null;

        //    //Act & Assert
        //    Assert.ThrowsException<ArgumentException>(() => new Feedback(feedbackName));
        //}
    }
}
