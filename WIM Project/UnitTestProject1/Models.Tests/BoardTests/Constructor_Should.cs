﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.BoardTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CorrectlyAssignPassedValues()
        {
            //Arrange
            string name = new string('s', 7);

            //Act
            var board = new Board(name);

            //Assert
            Assert.AreEqual(name, board.Name);

        }
    }
}
