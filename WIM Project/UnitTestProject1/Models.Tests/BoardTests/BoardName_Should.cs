﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.BoardTests
{
    [TestClass]
    public class BoardName_Should
    {
        [TestMethod]
        public void ThrowWhenPassedValueIsLessThanMin()
        {
            //Arrange
            var name = new string('s', 3);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Board(name));
        }

        [TestMethod]
        public void ThrowWhenPassedValueIsLessThanMax()
        {
            //Arrange
            var name = new string('s', 35);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Board(name));
        }

        [TestMethod]
        public void ThrowWhenPassedValueIsNull()
        {
            //Arrange
            string name = null;

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Board(name));
        }
    }
}
