﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace WIMProject.Tests.Models.Tests.BugTests
{
    [TestClass]
    public class Constructor_Should
    {
            [TestMethod]
            public void CorrectlyAssignPassedValues()
            {
                //Arrange
                string nameBug = new string('S', 11);               

                //Act
                var bug = new Bug(nameBug);               

                //Assert
                Assert.AreEqual(nameBug, bug.Title);
               
            }        
    }
}
