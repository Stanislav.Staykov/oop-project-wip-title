﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.AssingOrUnassing.Tests
{
    [TestClass]
    public class UnassignWorkItemToPersonCommandTest
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_UnassignWorkItemToPerson_With_CorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            var bugName = new string('S', 11);
            var bug = new Bug(bugName);
            database.Person.Add(person);
            database.WorkItems.Add(bug);
            database.Person.First(p => p.PersonName == "Personche").AddWorkItem(bug);

            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;
            List<string> param = new List<string>() { $"{bugId}", "Personche" };
            var sut = new UnassignWorkItemToPersonCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"Bug " +
                $"with ID {bugId} was unassigned from Personche";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_WrongInput()
        {
            //Arrange
           
            List<string> param = new List<string>() { $"1" };
            //Act
            var sut = new UnassignWorkItemToPersonCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Please enter valid parameters!", ex.Message);
        }


        [TestMethod]
        public void Execute_Should_ThrowException_When_WrogInput()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            var bugName = new string('S', 11);
            database.Person.Add(person);
            database.WorkItems.Add(new Bug(bugName));

            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;
            List<string> param = new List<string>() { $"bugId", "Personche" };

            //Act
            var sut = new UnassignWorkItemToPersonCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("ID must be a number!", ex.Message);
        }


        [TestMethod]
        public void Execute_Should_ThrowException_When_IdNotExist()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            var bugName = new string('S', 11);
            var bug = new Bug(bugName);
            database.Person.Add(person);
            database.WorkItems.Add(bug);
            database.Person.First(p => p.PersonName == "Personche").AddWorkItem(bug);

            List<string> param = new List<string>() { "2", "Personche" };
            var sut = new UnassignWorkItemToPersonCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("ID does not exist! Please provide valid ID.", ex.Message);
        }
      
        [TestMethod]
        public void Execute_Should_AssignWorkItemToPerson_With_CorrectInput_ShowHistory()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            var bugName = new string('S', 11);
            var bug = new Bug(bugName);
            database.Person.Add(person);
            database.WorkItems.Add(bug);
            database.Person.First(p => p.PersonName == "Personche").AddWorkItem(bug);

            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;
            List<string> param = new List<string>() { $"{bugId}", "Personche" };
            var sut = new UnassignWorkItemToPersonCommand(param);

            //Act
            sut.Execute();
            var expected = $"{ DateTime.Now}\n Personche was unassigned from Bug with ID {bugId}";
            var actual = database.Person.First(p => p.PersonName == "Personche").ActivityHistory.First().ToString().TrimEnd();

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
