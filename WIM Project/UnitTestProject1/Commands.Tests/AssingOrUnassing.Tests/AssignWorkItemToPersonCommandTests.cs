﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.AssingOrUnassing.Tests
{
    [TestClass]
    public class AssignWorkItemToPersonCommandTests
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_AssignWorkItemToPerson_With_CorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Boards.Add(board);

            var bug = new Bug("Testbug111");
            database.WorkItems.Add(bug);
            database.WorkItems.First(w => w.Title == "Testbug111").AddBoard(board.Name);
            database.Boards.First(b => b.Name == "Testboard").BoardItems.Add(bug);
            
            var person = new Person("Personche");
            database.Person.Add(person);

            var bugId = database.WorkItems.First(b => b.Title == "Testbug111").ID;
            List<string> param = new List<string>() { $"{bugId}", "Personche" };
            var sut = new AssignWorkItemToPersonCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"{database.WorkItems.First(wI => wI.ID == bugId).GetType().Name} " +
                $"with ID {bugId} was assigned to Personche";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_WrongInput()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            var bugName = new string('S', 11);
            database.Person.Add(person);
            database.WorkItems.Add(new Bug(bugName));

            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;
            List<string> param = new List<string>() { $"{bugId}" };
            var sut = new AssignWorkItemToPersonCommand(param);
            
            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Please enter valid parameters!", ex.Message);
        }


        [TestMethod]
        public void Execute_Should_ThrowException_When_WrogInput()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            var bugName = new string('S', 11);
            database.Person.Add(person);
            database.WorkItems.Add(new Bug(bugName));

            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;
            List<string> param = new List<string>() { $"bugId", "Personche" };
            var sut = new AssignWorkItemToPersonCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("ID must be a number!", ex.Message);
        }


        [TestMethod]
        public void Execute_Should_ThrowException_When_IdNotExist()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");            
            database.Person.Add(person);

            List<string> param = new List<string>() { "1", "Personche" };
            var sut = new AssignWorkItemToPersonCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("ID does not exist! Please provide a valid ID.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_PersonAlreadyWorking()
        {

            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            var bugName = new string('S', 11);
            database.Person.Add(person);
            database.WorkItems.Add(new Bug(bugName));
            database.Person.First(p => p.PersonName == "Personche").AddWorkItem(database.WorkItems.First(w => w.Title == bugName));

            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;
            List<string> param = new List<string>() { $"{bugId}", "Personche" };
            var sut = new AssignWorkItemToPersonCommand(param);


            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Person is already working on this item! Please provide another item!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_AssignWorkItemToPerson_With_CorrectInput_ShowHistory()
        {
            //Arrange
            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Boards.Add(board);

            var bug = new Bug("Testbug111");
            database.WorkItems.Add(bug);
            database.WorkItems.First(w => w.Title == "Testbug111").AddBoard(board.Name);
            database.Boards.First(b => b.Name == "Testboard").BoardItems.Add(bug);

            var person = new Person("Personche");
            database.Person.Add(person);

            var bugId = database.WorkItems.First(b => b.Title == "Testbug111").ID;
            List<string> param = new List<string>() { $"{bugId}", "Personche" };
            var sut = new AssignWorkItemToPersonCommand(param);

            //Act
            sut.Execute();
            var expected = $"{ DateTime.Now}\n Personche was assigned Bug with ID {bugId}";
            var actual = database.Person.First(p => p.PersonName == "Personche").ActivityHistory.First().ToString().TrimEnd();

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
