﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Core.Contracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Tests.Commands.Tests.CreateCommand.Tests.CreateBordCommand.Test
{
    [TestClass]
    public class CreateBordExecute_Should
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        } 

        [TestMethod]
        public void Execute_Should_CreateBoardWithCorrectlInput()
        {
            //Arrange         
            var team = new Team("Teamname");           
            Database.Instance.Teams.Add(team);

            List<string> param = new List<string>() {"Boardname", "Teamname" };
            var sut = new CreateBoardInTeamCommand(param);           

            //Act
            var actual = sut.Execute();
            var expected = $"Board Boardname was created in Team Teamname";

            //Assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_WrongInput()
        {
            //Arrange         
            var team = new Team("Teamname");
            Database.Instance.Teams.Add(team);
            List<string> param = new List<string>() { "Boardname" };
            var sut = new CreateBoardInTeamCommand(param);    

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Failed to parse CreateBoard command parameters.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_TeamNotExist()
        {
            //Arrange         
            var team = new Team("Teamname");
            Database.Instance.Teams.Add(team);
            List<string> param = new List<string>() { "Boardname", "Teamnames" };
            var sut = new CreateBoardInTeamCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Team with this name does not exist!", ex.Message);

        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_BoardExist()
        {
            //Arrange         
            var team = new Team("Teamname");
            var board = new Board("Boardname");
            Database.Instance.Teams.Add(team);
            Database.Instance.Boards.Add(board);
            Database.Instance.Teams.First(t => t.Name == "Teamname").TeamBoards.Add(board);
            List<string> param = new List<string>() { "Boardname", "Teamname" };
            var sut = new CreateBoardInTeamCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Board with this name already exists in this team!", ex.Message);

        }
        [TestMethod]
        public void WithCorrectlyAssignValues()
        {
            
            var team = new Team("TeamName");
            var board = new Board("BoardName");
            Database.Instance.Teams.Add(team);
            //Database.Instance.Boards.Add(board);

            //Assert
            Assert.AreEqual(0, Database.Instance.Boards.Count);
            Assert.AreEqual(1, Database.Instance.Teams.Count);

        }
    }
}
