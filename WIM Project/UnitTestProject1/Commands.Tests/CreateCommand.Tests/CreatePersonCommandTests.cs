﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.CreateCommand.Tests
{
    [TestClass]
    public class CreatePersonCommandTests
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_CreatePerson_With_CorrectInput()
        {
            //Arrange
            var personName = "Testperson";
            List<string> param = new List<string>() { $"{personName}" };
            var sut = new CreatePersonCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"Person Testperson was created";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_PersonExist()
        {
            //Arrange
            var personName = "Testperson";
            Database.Instance.Person.Add(new Person(personName));
            List<string> param = new List<string>() { $"{personName}" };
            var sut = new CreatePersonCommand(param);          

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual($"Person with name {personName} already exists.", ex.Message);
        }
    }
}
