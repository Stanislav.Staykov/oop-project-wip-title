﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Commands.Abstracts;
using WIMProject.Models.Contracts;

namespace WIMProject.Tests.Commands.Tests.CreateCommand.Tests.CreateTeamCommand.Test
{
    [TestClass]
    public class Execute_Should
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_CreateTeam_With_CorrectInput()
        {
            //Arrange
            var teamName = new string('s', 11);          
            List<string> param = new List<string>() {$"{teamName}"};
            var sut = new CreateTeamCommand111(param);        

            //Act
            var actual = sut.Execute();
            var expected = $"Team {Database.Instance.Teams.Last().Name} was created";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_With_WrongInput()
        {
            //Arrange
            var teamName = new string('s', 11);
            List<string> param = new List<string>();
            var sut = new CreateTeamCommand111(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Failed to parse CreateTeam command parameters.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_With_TeamExist()
        {
            //Arrange
            var teamName = new string('S', 11);
            Database.Instance.Teams.Add(new Team(teamName));
            List<string> param = new List<string>() { $"{teamName}" };
            var sut = new CreateTeamCommand111(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Team with this name already exists!", ex.Message);
        }
    }
}
