﻿using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;

namespace WIMProject.Tests.Commands.Tests.CreateCommand.Tests.CreateBugCommand.Tests
{
    [TestClass]
    public class CreateWorkItemCommandExecute_Should
    {
        [TestCleanup]
        public void TestCleanup()
        { 
            Database.Instance.Clear(); 
        }

        [TestMethod]
        public void CreateBugCommandExecuteCorrectlyAssignValues_Should()
        {
            //Arrange
            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);

            List<string> param = new List<string>() { "create bug", "Testbug1111111, Testboard" };
            var sut = new CreateWorkItemCommand(param);
           
            //Act
            var actual = sut.Execute();
            var expected = $"{Environment.NewLine}Bug {database.WorkItems.Last().Title} with ID: {database.WorkItems.Last().ID} was created. " +
                       $"{Environment.NewLine}" +
                       $"{Environment.NewLine}Please, describe the problem, starting your explanation with 'add description:'";

            //Assert
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void CreateStoryCommandExecuteCorrectlyAssignValues_Should()
        {
            //Arrange

            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);

            List<string> param = new List<string>() { "create story", "Teststory1111111, Testboard" };
            var sut = new CreateWorkItemCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"{Environment.NewLine}Story {database.WorkItems.Last().Title} with ID: {database.WorkItems.Last().ID} was created. " +
                            $"{Environment.NewLine}" +
                            $"{Environment.NewLine}Please, describe your case starting the description with 'add description:' or continue within the program.";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CreateFeedbackCommandExecuteCorrectlyAssignValues_Should()
        {
            //Arrange

            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);

            List<string> param = new List<string>() { "create feedback", "Feedback1111111, Testboard" };
            var sut = new CreateWorkItemCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"{Environment.NewLine}Feedback {database.WorkItems.Last().Title} with ID: {database.WorkItems.Last().ID} was created. " +
                            $"{Environment.NewLine}" +
                            $"{Environment.NewLine}Please, describe your experience starting the description with 'add description:' and rate it with 'add rating:' or continue within the program";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ReturnMessage_When_TheCommantIsWrong()
        {
            //Arrange

            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);

            List<string> param = new List<string>() { "wrong", "Feedback1111111, Testboard" };
            var sut = new CreateWorkItemCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"{Environment.NewLine}Could not parse CreateWorkItemCommand Parameters!";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_LessInputParams()
        {
            //Arrange

            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);

            List<string> param = new List<string>() { "create bug", "Testbug1111111" };
            var sut = new CreateWorkItemCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Failed to parse WorkItem command parameters.", ex.Message);
        }

        [TestMethod]   
        public void Execute_Should_ThrowException_When_BoardDoesNotExist()
        {
            //Arrange
            var database = Database.Instance;
            database.Teams.Add(new Team("Teamtest"));
            List<string> param = new List<string>() { "create bug", "Testbug1111111, Testboard" };
            var sut = new CreateWorkItemCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Board with this name does not exist! Please provide a valid board!", ex.Message);
        }

        [TestMethod]      
        public void Execute_Should_ThrowException_When_CreateBugWithThisNameExist()
        {
            //Arrange

            var database = Database.Instance;
            var board = new Board("Testboard");
            var bug = new Bug("Testbug11111");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.WorkItems.Add(bug);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.First(tb => tb.Name== "Testboard").BoardItems.Add(bug);

            List<string> param = new List<string>() { "create bug", "Testbug11111, Testboard" };
            var sut = new CreateWorkItemCommand(param);


            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Bug with this title already exists!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_CreateStoryWithThisNameExist()
        {
            //Arrange

            var database = Database.Instance;
            var board = new Board("Testboard");
            var story = new Story("Teststory11111");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.WorkItems.Add(story);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.First(tb => tb.Name == "Testboard").BoardItems.Add(story);

            List<string> param = new List<string>() { "create story", "Teststory11111, Testboard" };
            var sut = new CreateWorkItemCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Story with this title already exists!", ex.Message);
        }
        [TestMethod]
        public void Execute_Should_ThrowException_When_CreateFeedbackWithThisNameExist()
        {
            //Arrange

            var database = Database.Instance;
            var board = new Board("Testboard");
            var fb = new Feedback("Feedback11111");
            database.Teams.Add(new Team("Teamtest"));
            database.Boards.Add(board);
            database.WorkItems.Add(fb);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.Add(board);
            database.Teams.First(t => t.Name == "Teamtest").TeamBoards.First(tb => tb.Name == "Testboard").BoardItems.Add(fb);

            List<string> param = new List<string>() { "create feedback", "Feedback11111, Testboard" };
            var sut = new CreateWorkItemCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Feedback with this title already exists!", ex.Message);
        }
    }
}
