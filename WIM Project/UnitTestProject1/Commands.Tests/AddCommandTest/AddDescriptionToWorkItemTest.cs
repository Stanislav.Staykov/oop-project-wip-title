﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;

namespace WIMProject.Tests.Commands.Tests.AddCommandTest
{
    [TestClass]
    public class AddDescriptionToWorkItemTest
    {

        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_AddDescription_With_CorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var description = new string('S', 111);
            var bugName = "Trichasaisamogrdi";
            var bug = new Bug(bugName);
            Database.Instance.WorkItems.Add(bug);
            var itemId = database.WorkItems.First(f => f.Title == bugName).ID;
            List<string> param = new List<string>() { $"{description}" };
            var sut = new AddDescriptionCommand(param);
            var expected = $"Description added to {database.WorkItems.Last().GetType().Name} with ID {database.WorkItems.Last().ID}";

            //Act
            var actual = sut.Execute();

            //Assert          
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_WrongInput()
        {
            //Arrange
            var database = Database.Instance;
            var description = new string('S', 9);
            var bugName = "Trichasaisamogrdi";
            var bug = new Bug(bugName);
            Database.Instance.WorkItems.Add(bug);
            var itemId = database.WorkItems.First(f => f.Title == bugName).ID;
            List<string> param = new List<string>() { $"{description}" };
            var sut = new AddDescriptionCommand(param);
           
            //Assert          
            var ex=Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("The description must be at least 10, and less than 500 characters!", ex.Message);
        }
    }
}
