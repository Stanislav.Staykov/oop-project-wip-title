﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.AddCommandTest
{
    [TestClass]
    public class AddPersonToTeamTest
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_AddPersonToTeam_With_CorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            database.Person.Add(person);
            database.Teams.Add(new Team("Teamtest"));

            List<string> param = new List<string>() { "Personche", "Teamtest" };
            var sut = new AddPersonToTeamCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"Personche was added to team Teamtest.";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_PersonNotExist()
        {
            //Arrange
            var database = Database.Instance;
            database.Teams.Add(new Team("Teamtest"));

            List<string> param = new List<string>() { "Personche", "Teamtest" };
            var sut = new AddPersonToTeamCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Person with name: Personche does not exists.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_TeamNotExist()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            database.Person.Add(person);

            List<string> param = new List<string>() { "Personche", "Teamtest" };
            var sut = new AddPersonToTeamCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Team with name: Teamtest does not exists.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_PersonIsAPartOfTeam()
        {
            //Arrange
            var database = Database.Instance;
            var person = new Person("Personche");
            database.Person.Add(person);
            database.Teams.Add(new Team("Teamtest"));
            database.Teams.First(t => t.Name == "Teamtest").TeamMembers.Add(person);

            List<string> param = new List<string>() { "Personche", "Teamtest" };
            var sut = new AddPersonToTeamCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Personche is already part of this team!", ex.Message);
        }
    }
}
