﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Commands.Abstracts;
using WIMProject.Core;
using WIMProject.Models.Contracts;

namespace WIMProject.Tests.Commands.Tests
{
    [TestClass]
    public class AddCommentToWorkItem_Should
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void CorrectlyAssignValues()
        {          
            //Arrange
            var database = Database.Instance; 
            var comment = "sss";
            var bugName = "Trichasaisamogrdi";
            var bug = new Bug(bugName);
            Database.Instance.WorkItems.Add(bug);
            var itemId = database.WorkItems.First(f => f.Title == bugName).ID;
            List<string> param = new List<string>() { $"{itemId}", $"{comment}" };
            var sut = new AddCommentCommand(param);
            var expected = $"Comment added to Bug with ID 1";

            //Act
            var actual = sut.Execute();          

            //Assert          
            Assert.AreEqual(expected, actual);         

        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_IdIsNotNumber()
        {
            //Arrange          
            List<string> param = new List<string>() { $"wrong", "{comment}" };
            var sut = new AddCommentCommand(param);   

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("ID must be a number!", ex.Message);

        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_WorkItemDoesNotExist()
        {
            //Arrange          
            List<string> param = new List<string>() { $"1", "{comment}" };
            var sut = new AddCommentCommand(param);

            //Assert          
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Item with ID: 1 does not exist.", ex.Message);

        }

    }
}
