﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;

namespace WIMProject.Tests.Commands.Tests.ChangeCommandTests
{
    [TestClass]
    public class ChangeBugOptionCommandTest
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_ChangeBugPriority_With_PriorityCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Priority", "Medium" };
            var sut = new ChangeBugOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Bug's Priority was changed to Medium";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ChangeBugSeverity_With_SeverityCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Severity", "Major" };
            var sut = new ChangeBugOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Bug's Severity was changed to Major";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ChangeBugStatus_With_StatusCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Status", "Fixed" };
            var sut = new ChangeBugOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Bug's Status was changed to Fixed";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongInput()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Status" };
            var sut = new ChangeBugOptionCommand(param);
         
            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid parameters count.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongTypeId()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { "{bugId}", "Status", "Fixed" };
            var sut = new ChangeBugOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("It is not number.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_NotBug()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Story(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Status", "Fixed" };
            var sut = new ChangeBugOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual($"The item with ID: {bugId} is not a Bug!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongPriority()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Priority", "Fixeds" };
            var sut = new ChangeBugOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid priority type", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongSeverity()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Severity", "Fixeds" };
            var sut = new ChangeBugOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid severity type", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongStatus()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "Status", "Fixeds" };
            var sut = new ChangeBugOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid status type", ex.Message);
        }


        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongCommand()
        {
            //Arrange
            var database = Database.Instance;
            var bugName = "Testbug111111111";
            var bug = new Bug(bugName);
            database.WorkItems.Add(bug);
            var bugId = database.WorkItems.First(b => b.Title == bugName).ID;

            List<string> param = new List<string>() { $"{bugId}", "statusssssss", "Fixeds" };
            var sut = new ChangeBugOptionCommand(param);

            //Assert            
            Assert.AreEqual("Please provide a valid property to change!", sut.Execute());
        }
    }        
}
