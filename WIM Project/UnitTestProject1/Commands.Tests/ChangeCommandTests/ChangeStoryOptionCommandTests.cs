﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;

namespace WIMProject.Tests.Commands.Tests.ChangeCommandTests
{
    [TestClass]
    public class ChangeStoryOptionCommandTests
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_ChangeBugPriority_With_PriorityCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Priority", "Medium" };
            var sut = new ChangeStoryOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Story's Priority was changed to Medium";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ChangeStorySeverity_With_SizeCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Size", "Medium" };
            var sut = new ChangeStoryOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Story's Size was changed to Medium";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ChangeStoryStatus_With_StatusCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Status", "InProgress" };
            var sut = new ChangeStoryOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Story's Status was changed to InProgress";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongInput()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Status" };
            var sut = new ChangeStoryOptionCommand(param);
           
            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid parameters count.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongTypeId()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { "{storyId}", "Status", "InProgress" };
            var sut = new ChangeStoryOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("ID must be a number!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_NotStory()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Feedback(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Status", "InProgress" };
            var sut = new ChangeStoryOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual($"The item with ID: {storyId} is not a Story!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongPriority()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Priority", "InProgress" };
            var sut = new ChangeStoryOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid priority type", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongStatus()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Status", "InProgresssssss" };
            var sut = new ChangeStoryOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid status type", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongSize()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Size", "InProgress" };
            var sut = new ChangeStoryOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid Size type", ex.Message);
        }


        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongCommand()
        {
            //Arrange
            var database = Database.Instance;
            var storyName = "Teststory111111111";
            var story = new Story(storyName);
            database.WorkItems.Add(story);
            var storyId = database.WorkItems.First(b => b.Title == storyName).ID;

            List<string> param = new List<string>() { $"{storyId}", "Statusss", "InProgress" };
            var sut = new ChangeStoryOptionCommand(param);

            //Assert            
            Assert.AreEqual("Please provide a valid property to change!", sut.Execute());
        }
    }
}
