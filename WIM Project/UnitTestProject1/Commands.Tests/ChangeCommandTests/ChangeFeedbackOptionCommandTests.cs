﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;

namespace WIMProject.Tests.Commands.Tests.ChangeCommandTests
{
    [TestClass]
    public class ChangeFeedbackOptionCommandTests
    {

        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_ChangeFeedbackPriority_With_RatingCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var feedbackName = "Testfeedback111111111";
            var feedback = new Feedback(feedbackName);
            database.WorkItems.Add(feedback);
            var feedbackId = database.WorkItems.First(b => b.Title == feedbackName).ID;

            List<string> param = new List<string>() { $"{feedbackId}", "Rating", "9" };
            var sut = new ChangeFeedbackOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Feedback's Rating was changed to 9";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ChangeFeedbackPriority_With_StatusCorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var feedbackName = "Testfeedback111111111";
            var feedback = new Feedback(feedbackName);
            database.WorkItems.Add(feedback);
            var feedbackId = database.WorkItems.First(b => b.Title == feedbackName).ID;

            List<string> param = new List<string>() { $"{feedbackId}", "Status", "Done" };
            var sut = new ChangeFeedbackOptionCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"The Feedback's Status was changed to Done";

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongInput()
        {
            //Arrange          
            List<string> param = new List<string>() { $"1", "Status" };

            //Act
            var sut = new ChangeFeedbackOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid parameters count.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_IdNotNumber()
        {
            ////Arrange           
            List<string> param = new List<string>() { "{feedbackId}", "Status", "Done" };

            //Act
            var sut = new ChangeFeedbackOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("ID it is not number.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_IdDontExist()
        {
            //Arrange
            var database = Database.Instance;
            var feedbackName = "Testfeedback111111111";
            var feedback = new Feedback(feedbackName);            
            List<string> param = new List<string>() { $"2", "Status", "Done" };

            //Act
            var sut = new ChangeFeedbackOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual($"Feedback with ID: 2, does not exist!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongRatingInput()
        {
            //Arrange
            var database = Database.Instance;
            var feedbackName = "Testfeedback111111111";
            var feedback = new Feedback(feedbackName);
            database.WorkItems.Add(feedback);
            var feedbackId = database.WorkItems.First(b => b.Title == feedbackName).ID;
            List<string> param = new List<string>() { $"{feedbackId}", "Rating", "Done" };

            //Act
            var sut = new ChangeFeedbackOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual($"Done must be a number!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongStatusInput()
        {
            //Arrange
            var database = Database.Instance;
            var feedbackName = "Testfeedback111111111";
            var feedback = new Feedback(feedbackName);
            database.WorkItems.Add(feedback);
            var feedbackId = database.WorkItems.First(b => b.Title == feedbackName).ID;
            List<string> param = new List<string>() { $"{feedbackId}", "Status", "Done1" };

            //Act
            var sut = new ChangeFeedbackOptionCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual($"Done1 it's not a part of FeedbackStatusType!", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowExceptions_When_WrongInputOptions()
        {
            //Arrange
            var database = Database.Instance;
            var feedbackName = "Testfeedback111111111";
            var feedback = new Feedback(feedbackName);
            database.WorkItems.Add(feedback);
            var feedbackId = database.WorkItems.First(b => b.Title == feedbackName).ID;
            List<string> param = new List<string>() { $"{feedbackId}", "Status1", "Done" };

            //Act
            var sut = new ChangeFeedbackOptionCommand(param);

            //Assert            
            Assert.AreEqual($"Please provide a valid property to change!", sut.Execute());
        }
    }
}
