﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.ShowCommandTests
{
    [TestClass]
    public class ShowAllTeamMembersCommandTests
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_ShowAllTeamMembers_With_CorrectInput()
        {
            //Arrange
            var database = Database.Instance;
            var team1Name = "Testteam1";
            var team2Name = "Testteam2";
            database.Teams.Add(new Team(team1Name));
            database.Teams.Add(new Team(team2Name));
            database.Teams.First(t => t.Name == team1Name).TeamMembers.Add(new Person("Testperson1"));
            database.Teams.First(t => t.Name == team1Name).TeamMembers.Add(new Person("Testperson2"));

            List<string> param = new List<string>() { $"{team1Name}" };
            var sut = new ShowAllTeamMembersCommand(param);

            //Act
            var actual = sut.Execute();

            var expected = string.Join($"{Environment.NewLine}", database.Teams.First(t => t.Name == team1Name).TeamMembers.Select(tm => tm.PersonName).ToList());



            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_WrongInput()
        {
            //Arrange           
            List<string> param = new List<string>();

            //Act
            var sut = new ShowAllTeamMembersCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual("Invalid parameters count.", ex.Message);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_NoTeams()
        {
            var database = Database.Instance;
            var team1Name = "Testteam1";           

            List<string> param = new List<string>() { $"{team1Name}" };
            
            //Act
            var sut = new ShowAllTeamMembersCommand(param);

            //Assert
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.AreEqual($"Team with name: {team1Name}, does not exist!", ex.Message);
        }
    }
}
