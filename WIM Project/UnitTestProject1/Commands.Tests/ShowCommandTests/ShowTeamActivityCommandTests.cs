﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.ShowCommandTests
{
    [TestClass]
    public class ShowTeamActivityCommandTests
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_ShowTeamActivity_When_HaveActivity()
        {
            //Arrange
            var database = Database.Instance;
            var board = new Board("Testboard");
            var team = new Team("Testteam");
            var bug = new Bug("Testbug111");
            var person = new Person("Personche");

            database.Teams.Add(team);
            database.Boards.Add(board);
            database.Person.Add(person);
            database.WorkItems.Add(bug);


            database.WorkItems.First(w => w.Title == "Testbug111").AddBoard(board.Name);
            database.Boards.First(b => b.Name == "Testboard").BoardItems.Add(bug);
            database.Teams.First(t => t.Name == "Testteam").TeamBoards.Add(board);
            database.Teams.First(t => t.Name == "Testteam").TeamMembers.Add(person);


            var bugId = database.WorkItems.First(b => b.Title == "Testbug111").ID;
            List<string> param = new List<string>() { $"{bugId}", "Personche" };
            List<string> param1 = new List<string>() { "Testteam" };
            
            var sut = new AssignWorkItemToPersonCommand(param);
            var sut1 = new ShowTeamActivityCommand(param1);


            //Act
            sut.Execute();
            var actual = sut1.Execute();

            var sb = new StringBuilder();

            foreach (var persons in database.Teams.First(t => t.Name == "Testteam").TeamMembers)
            {
                sb.Append(new ShowPersonsActivityCommand(new List<string> { persons.PersonName }).Execute());
            }

            var expected = sb.ToString().Trim();


            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ShowPersonActivity_When_HasNoActivity()
        {
            //Arrange
            var database = Database.Instance;
            var board = new Board("Testboard");
            database.Boards.Add(board);

            var bug = new Bug("Testbug111");
            database.WorkItems.Add(bug);
            database.WorkItems.First(w => w.Title == "Testbug111").AddBoard(board.Name);
            database.Boards.First(b => b.Name == "Testboard").BoardItems.Add(bug);

            var person = new Person("Personche");
            database.Person.Add(person);

            var bugId = database.WorkItems.First(b => b.Title == "Testbug111").ID;
            List<string> param = new List<string>() { $"{bugId}", "Personche" };
            List<string> param1 = new List<string>() { $"Personche" };
            // var sut = new AssignWorkItemToPersonCommand(param);
            var sut1 = new ShowPersonsActivityCommand(param1);


            //Act
            // sut.Execute();
            var actual = sut1.Execute();

            //var sb = new StringBuilder();
            //foreach (var ev in database.Person.First(p => p.PersonName == "Personche").ActivityHistory)
            //{
            //    sb.AppendLine();
            //    sb.AppendLine($"* {ev}");
            //}

            //var expected = $"{Environment.NewLine}{"Personche"} has worked on:{Environment.NewLine}{sb.ToString().TrimEnd()}";
            var expected = $"{database.Person.First(p => p.PersonName == "Personche")} has no activity history!";


            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
