﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.ShowCommandTests
{
    [TestClass]
    public class ShowAllTeamsCommandTests
    {

        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_ShowAllTeams_With_CorrectInput()
        {
            //Arrange
            var team1Name = "Testteam1";
            var team2Name = "Testteam2";
            Database.Instance.Teams.Add(new Team(team1Name));
            Database.Instance.Teams.Add(new Team(team2Name));
            List<string> param = new List<string>();
            var sut = new ShowAllTeamsCommand(param);

            //Act
            var actual = sut.Execute();
            var sb = new StringBuilder();
            foreach (var team in Database.Instance.Teams)
            {
                sb.Append("* ");
                sb.AppendLine(team.Name);
            }
            
            var expected = sb.ToString(); ;


            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_NoTeams()
        {
            //Arrange           
            List<string> param = new List<string>();
            var sut = new ShowAllTeamsCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"There are no registered teams.";


            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
