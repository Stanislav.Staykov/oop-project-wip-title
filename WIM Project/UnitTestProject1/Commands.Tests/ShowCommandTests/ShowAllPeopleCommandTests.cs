﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMProject.Commands;
using WIMProject.Core;
using WIMProject.Models;

namespace WIMProject.Tests.Commands.Tests.ShowCommandTests
{
    [TestClass]
    public class ShowAllPeopleCommandTests
    {
        [TestCleanup]
        public void TestCleanup()
        {
            Database.Instance.Clear();
        }

        [TestMethod]
        public void Execute_Should_ShowAllPerson_With_CorrectInput()
        {
            //Arrange
            var person1Name = "Testperson1";
            var person2Name = "Testperson2";
            Database.Instance.Person.Add(new Person(person1Name));
            Database.Instance.Person.Add(new Person(person2Name));
            List<string> param = new List<string>();
            var sut = new ShowAllPeopleCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"Testperson1{ Environment.NewLine}Testperson2"; 


            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Execute_Should_ThrowException_When_NoPerson()
        {
            //Arrange           
            List<string> param = new List<string>();
            var sut = new ShowAllPeopleCommand(param);

            //Act
            var actual = sut.Execute();
            var expected = $"There are no registered people.";


            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
